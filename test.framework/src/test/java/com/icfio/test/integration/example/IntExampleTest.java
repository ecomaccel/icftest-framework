package com.icfio.test.integration.example;

import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.icfio.test.integration.IntegrationTestBase;
import com.icfio.test.integration.dataprovider.DataProviderFactory;

/**
 * The class which contains example how to use DataProvider
 * for TestNG tests.
 * 
 */
@Test(dataProviderClass = DataProviderFactory.class)
public final class IntExampleTest extends IntegrationTestBase {

    private static final Logger LOG = Logger.getLogger(IntExampleTest.class);
    
    /**
     * Test ICF yahoo link.
     */
    @Test
    public void testICFYahooLink() {
        
        driver.get("http://www.yahoo.com");
        
        driver.findElement(By.name("p")).clear();

        driver.findElement(By.name("p")).sendKeys("ICF");
		driver.findElement(By.id("UHSearchWeb")).click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("ICF")));

        driver.findElement(By.linkText("ICF")).click();

        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("footer_copyright")));

        /* validate response */
        String result = driver.findElement(By.id("footer_copyright")).getText();
        LOG.debug("response:" + result);

        String expectedText = "ICF International, Inc. All Rights Reserved.";
        LOG.debug("Expected text in the response:" + expectedText);

        /* if assert fails then all depended tests must not running. */
        Assert.assertTrue(StringUtils.contains(result, expectedText));

    }
    
    /**
     * Test applications.
     * 
     * @param dpMap
     *            the dp map
     */
    @Test(dataProvider = "csv-data-provider")
    public void testICFLink(HashMap<String, String> dpMap) {
        
        driver.get(dpMap.get("startUrl"));
        
        driver.findElement(By.name(dpMap.get("inputLoc"))).clear();
        driver.findElement(By.name(dpMap.get("inputLoc"))).sendKeys(dpMap.get("inputValue"));
        driver.findElement(By.id(dpMap.get("submitLoc"))).click();

        
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(dpMap.get("nextLinkLoc"))));

        driver.findElement(By.linkText(dpMap.get("nextLinkLoc"))).click();

        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(dpMap.get("outputLoc"))));

        /* validate response */
        String result = driver.findElement(By.id(dpMap.get("outputLoc"))).getText();
        LOG.debug("response:" + result);

        String expectedText = dpMap.get("expectedText");
        LOG.debug("Expected text in the response: " + expectedText);

        /* if assert fails then all depended tests must not running. */
        Assert.assertTrue(StringUtils.contains(result, result));
    }
    
    @Override
    public Logger getLog() {
        return LOG;
    }
    
    

}
