package com.cgi.test;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.icfio.test.pagefactory.Cart;
import com.icfio.test.pagefactory.CheckoutSummary;
import com.icfio.test.pagefactory.Login;


/**
 * This example in JUnit just to show differences with TestNG
 *
 */
public class SimpleChekoutTest {

	private WebDriver driver;
	private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {

		driver = new FirefoxDriver();
		
		baseUrl = "https://dealer-local.liftmaster.com/";
		//baseUrl = "https://dealer-dev.liftmaster.com/";
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testCheckout() throws Exception {
		// login
		Login login = new Login(driver, baseUrl);
		login.loginToB2Bsite("agdev0000100037@mailinator.com", "12341234");
	
		// add product to cart by using quick order
		Cart cart = new Cart(driver, baseUrl);
		cart.quickOrderAddToCart("8500");
		// cart page 
		cart.openCartPage();
		cart.clickCheckout();

		// checkout page
		CheckoutSummary checkoutSummary = new CheckoutSummary(driver);
		checkoutSummary.simpleCheckout();

		// wait for confirmation page
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("print")));
		Assert.assertEquals("Order Confirmation | Liftmaster Dealer Extranet", driver.getTitle());
	}



	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}




}
