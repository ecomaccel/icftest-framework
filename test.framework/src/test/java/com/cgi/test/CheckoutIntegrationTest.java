package com.cgi.test;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.icfio.test.integration.IntegrationTestBase;
import com.icfio.test.integration.dataprovider.DataProviderFactory;
import com.icfio.test.integration.example.IntExampleTest;
import com.icfio.test.pagefactory.Cart;
import com.icfio.test.pagefactory.CheckoutSummary;
import com.icfio.test.pagefactory.Login;

@Test(dataProviderClass = DataProviderFactory.class)
public class CheckoutIntegrationTest extends IntegrationTestBase {

	private static final Logger LOG = Logger.getLogger(IntExampleTest.class);

	@Override
	public Logger getLog() {
		return LOG;
	}

	@Test(dataProvider = "props-data-provider")
	public void testCheckout(HashMap<String, String> dpMap) throws Exception {
		// login
		Login login = new Login(driver, seleniumUrl);
		login.loginToB2Bsite(dpMap.get("username"), dpMap.get("password"));

		// add product to cart by using quick order
		Cart cart = new Cart(driver, seleniumUrl);
		cart.quickOrderAddToCart(dpMap.get("materialId"));
		// cart page
		cart.openCartPage();
		cart.clickCheckout();

		// checkout page
		CheckoutSummary checkoutSummary = new CheckoutSummary(driver);
		checkoutSummary.simpleCheckout();

		// wait for confirmation page
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("print")));
		Assert.assertTrue(driver.getTitle().contains("Order Confirmation"));

		login.logout();
	}

	
	
}
