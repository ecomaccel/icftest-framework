package com.cgi.test;

import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.icfio.test.integration.IntegrationTestBase;
import com.icfio.test.integration.dataprovider.DataProviderFactory;
import com.icfio.test.integration.example.IntExampleTest;
import com.icfio.test.pagefactory.Login;

@Test(dataProviderClass = DataProviderFactory.class)
public class LoginIntegrationTest extends IntegrationTestBase {

	private static final Logger LOG = Logger.getLogger(IntExampleTest.class);

	@Override
	public Logger getLog() {
		return LOG;
	}

	@Test(dataProvider = "props-data-provider")
	public void testLogin(HashMap<String, String> dpMap) throws Exception {
		// login
		Login login = new Login(driver, seleniumUrl);
		login.loginToB2Bsite(dpMap.get("username"), dpMap.get("password"));

		// assert for home page
		Assert.assertTrue(driver.getTitle().contains("Liftmaster Dealer Extranet"));
	}

}
