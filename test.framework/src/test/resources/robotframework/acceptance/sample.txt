*** Settings ***
Library           Selenium2Library

*** Test Cases ***
Sample Test
    [Documentation]    Sample test to open browser with google url , check for input field and close browser
    Open Browser    http://google.com
    Wait Until Element Is Visible    name=q
    Close Browser
