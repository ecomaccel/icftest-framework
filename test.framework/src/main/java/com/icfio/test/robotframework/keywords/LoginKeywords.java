package com.icfio.test.robotframework.keywords;

import org.openqa.selenium.WebDriver;
import org.robotframework.javalib.annotation.ArgumentNames;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.testng.log4testng.Logger;

import com.icfio.test.pagefactory.Login;
import com.icfio.test.robotframework.Selenium2hybrisLibrary;

@RobotKeywords
public class LoginKeywords {

    private static final Logger LOG = Logger.getLogger(LoginKeywords.class);

    @RobotKeyword
    @ArgumentNames({ "username", "password" })
    public void login(String username, String password) {

	// check current webDriver
	WebDriver driver = Selenium2hybrisLibrary.getCurrentBrowser();

	if (driver != null) {
	    String url = driver.getCurrentUrl();
	    String baseUrl = url.substring(0, url.indexOf("/", 9));

	    if (LOG.isDebugEnabled()) {
		LOG.debug("Current webDriver url =" + driver.getCurrentUrl());
		LOG.debug("Base url =" + baseUrl);
	    }

	    Login login = new Login(driver, baseUrl);
	    login.loginToB2Bsite(username, password);
	} else {
	    LOG.error("Can't get current WebDriver!");
	}
    }

}
