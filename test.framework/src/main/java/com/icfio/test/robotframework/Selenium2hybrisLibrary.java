package com.icfio.test.robotframework;

import java.lang.reflect.Field;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.openqa.selenium.WebDriver;
import org.robotframework.javalib.library.AnnotationLibrary;

import com.github.markusbernhardt.selenium2library.Selenium2Library;
import com.github.markusbernhardt.selenium2library.keywords.BrowserManagement;
import com.github.markusbernhardt.selenium2library.utils.WebDriverCache;

public class Selenium2hybrisLibrary extends AnnotationLibrary {

	/**
	 * This means the same instance of this class is used throughout the life
	 * cycle of a Robot Framework test execution.
	 */
	public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

	/**
	 * The list of keyword patterns for the AnnotationLibrary
	 */
	public static final String KEYWORD_PATTERN = "com/icfio/test/robotframework/keywords/**/*.class";

	public Selenium2hybrisLibrary() {
		super(KEYWORD_PATTERN);
		createKeywordFactory(); // => init annotations
	} 
	
	public static WebDriverCache getWebDriverCache() {
		try {
			Selenium2Library selenium2Library = (Selenium2Library) getLibraryInstance("Selenium2Library");
			BrowserManagement bm = selenium2Library.getBrowserManagement();
			Field cacheField = BrowserManagement.class.getDeclaredField("webDriverCache");
			cacheField.setAccessible(true);
			return (WebDriverCache) cacheField.get(bm);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static WebDriver getCurrentBrowser() {
		return getWebDriverCache().getCurrent();
	}

	private static Object getLibraryInstance(String library) throws ScriptException {
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("python");
		engine.put("library", library);
		engine.eval("from robot.libraries.BuiltIn import BuiltIn");
		engine.eval("instance = BuiltIn().get_library_instance(library)");
		return engine.get("instance");
	}

}
