package com.icfio.test.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Cart {
	WebDriver driver;
	private String baseUrl;

	@FindBy(linkText = "QUICK ORDER")
	WebElement quickOrderLink;

	@FindBy(id = "quickOrderSearch")
	WebElement quickOrderSearch;

	@FindBy(css = "button.positive")
	WebElement quickOrderAddToCartButton;

	@FindBy(xpath = "//button[@type='button']")
	// @FindBy(css = "checkoutButton positive right")
	WebElement checkoutButton;

	public Cart(WebDriver driver, String baseUrl) {
		this.driver = driver;
		this.baseUrl = baseUrl;
		// Wait 60 Second To Find Element If Element Is Not Present
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
	}

	public void openCartPage() {
		this.driver.get(baseUrl + "/cart");
	}

	public void clickCheckout() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(checkoutButton));

		this.checkoutButton.click();
	}

	public void setQuickOrderSearch(String quickOrderSearchString) {
		// quickOrderSearch.click();
		quickOrderSearch.clear();
		quickOrderSearch.sendKeys(quickOrderSearchString);
	}

	public void quickOrderAddToCart(String modelNumber) {
		// open quick order window
		this.quickOrderLink.click();

		// enter model number
		this.setQuickOrderSearch(modelNumber);

		// add to cart
		this.quickOrderAddToCartButton.click();
	}

}
