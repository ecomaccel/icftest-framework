package com.icfio.test.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class Login {
	WebDriver driver;
	private String baseUrl;
	
	@FindBy(id = "j_username")
	WebElement userName;

	@FindBy(id = "j_password")
	WebElement password;
	
	@FindBy(css = "button.form.right")
	WebElement loginButton;
	
	@FindBy(linkText = "Logout")
	WebElement logoutLink;

	public Login(WebDriver driver, String baseUrl) {
		this.driver = driver;
		this.baseUrl = baseUrl;
		//  Wait 60 Second To Find Element If Element Is Not Present
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
	}

	public void setUserName(String userNameString) {
		userName.clear();
		userName.sendKeys(userNameString);
	}

	public void setPassword(String passwordString) {
		password.clear();
		password.sendKeys(passwordString);
	}

	public void openLoginPage() {
		driver.get(baseUrl + "/login");
	}
	
	public void logout() {
		this.logoutLink.click();
	}

	public void loginToB2Bsite(String userNameString, String passwordString) {
		this.openLoginPage();
		
		// Fill user name
		this.setUserName(userNameString);
		
		//Fill password
		this.setPassword(passwordString);
		
		// click login
		loginButton.click();
	}
	
}
