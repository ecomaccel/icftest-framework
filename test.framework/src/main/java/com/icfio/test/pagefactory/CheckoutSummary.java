package com.icfio.test.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CheckoutSummary {
	WebDriver driver;

	@FindBy(id = "PurchaseOrderNumber")
	WebElement purchaseOrderNumber;

	@FindBy(id = "AttentionOf")
	WebElement attentionOf;

	@FindBy(xpath = "(//button[@type='button'])[3]")
	WebElement placeOrderButton;

	public CheckoutSummary(WebDriver driver) {
		this.driver = driver;
		// Wait 60 Second To Find Element If Element Is Not Present
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
	}

	public void setPurchaseOrderNumber(String numberText) {
		purchaseOrderNumber.sendKeys(numberText);
	}

	public void setAttentionOf(String attentionOfText) {
		attentionOf.sendKeys(attentionOfText);
	}

	public void clickPlaceOrder() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(placeOrderButton));

		this.placeOrderButton.click();
	}

	public void simpleCheckout() {
		this.setPurchaseOrderNumber("111");
		this.setAttentionOf("222");

		this.clickPlaceOrder();
	}

}
