/**
 * 
 */
package com.icfio.test.integration.constant;

/**
 * Browser types supported by the WebDriver factory.
 * 
 * All available browser drivers for Selenium can be found here: http://www.seleniumhq.org/download/
 * 
 * For automatic download and set system properties executable drivers should be defined in the file :
 * src\test\resources\drivers\RepositoryMap.xml (look for documentation on
 * https://github.com/Ardesco/selenium-standalone-server-plugin/blob/master/README.md )
 */
public enum BrowserTypeE {
	IE, FF, HtmlUnit, Chrome, Opera;
}
