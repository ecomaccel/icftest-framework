package com.icfio.test.integration;

import static com.icfio.test.integration.IntegrationTestUtil.DEFAULT_URL;

import java.lang.reflect.Method;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.log4testng.Logger;

import com.icfio.test.integration.constant.TestSuiteKeyNameE;
import com.icfio.test.integration.driver.WebDriverFactory;

/**
 * Base test class for all integration tests. 
 * 
 * Initializes Selenium Web Driver based on the context of the running testNG Suite (xml). 
 * Contains methods to decorate each test method with log. 
 * May contains other useful methods with access to driver or seleniumUrl.
 * 
 * @author Andrey Gotchalk
 */
public abstract class IntegrationTestBase {

    /**
     * Test NG logger.
     */
    private static final Logger LOG = Logger.getLogger(IntegrationTestBase.class);

    /**
     * Selenium WebDriver.
     */
    public static WebDriver driver;
    
	public static String seleniumUrl;

    /** 
     * Selenium ENTER_KEY. 
     */
    public static final String ENTER_KEY = "\n";
    
    
    /**
     * Navigate to test initial page.
     * 
     * @param forceRefresh
     *            the force refresh
     * @param url
     *            the url
     */
    private static void navigateToTestInitialPage(WebDriver driver, final boolean forceRefresh, final String url) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("forceRefresh: " + forceRefresh + ", url: " + url);
        }

        if (forceRefresh || driver.getCurrentUrl().contains(url) == false) {
            driver.get(url);
            
            /* fix of certificate issue for IE */ 
            if (driver instanceof InternetExplorerDriver) {
				while (isElementPresent(By.id("overridelink"))) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Try to click 'Continue to this website (not recommended)'");
					}
					/* Try to click "Continue to this website (not recommended)." */
					/* it must work for IE7 - IE11 */
                    driver.navigate().to("javascript:document.getElementById('overridelink').click()");
				}
            }
            
            /* debug of RemoteWebDriver connection */ 
            if (LOG.isDebugEnabled() && driver instanceof RemoteWebDriver) {
                LOG.debug("Current browser title: " + driver.getTitle());
                RemoteWebDriver remoteWebDriver = (RemoteWebDriver) driver;
                LOG.debug("Current selenium session id: " + remoteWebDriver.getSessionId());
            }
        }
    }

    /**
     * Initializes WebDriver based on test suite context.
     * Default tests will use local FireFox web driver.
     * 
     * @param context
     *            - This class defines a test context which contains all the
     *            information for a given test run. An instance of this context
     *            is passed to the test listeners so they can query information
     *            about their environment.
     */
    @BeforeSuite
    public static void init(ITestContext context) {
		seleniumUrl = context.getCurrentXmlTest().getParameter(TestSuiteKeyNameE.SELENIUM_URL.getKeyName());

        if (StringUtils.isEmpty(seleniumUrl)) {
            seleniumUrl = DEFAULT_URL;
        }
   
        try {
            driver = WebDriverFactory.createWebDriver(context);
        } catch (WebDriverException wdEx) {
            LOG.error("Errors creating Web Driver", wdEx);
        }
        
		navigateToTestInitialPage(driver, true, seleniumUrl);
    }
    
 
    /**
     * Log decoration for the start of each test method.
     * 
     * @param method
     *            the method
     */
    @BeforeMethod
    public final void logStartMethod(Method method) {
        if (getLog() != null) {
            final String logIntro = getShortTestMethodName(method) + " starting...";
            getLog().info("------------- " + logIntro + "-------------");
        }
        
     }

    /**
     * Log decoration for the end of each test method.
     * 
     * @param method
     *            the method
     */
    @AfterMethod
    public final void logEndMethod(Method method) {
        if (getLog() != null) {
            final String logIntro = getShortTestMethodName(method) + " finished.  ";
            getLog().info("------------- " + logIntro + "-------------");
        }
        
    }

    /**
     * The method returns name of testNG test class from method object.
     * 
     * @param method
     *            the method
     * @return String class name
     */
    private String getShortTestMethodName(final Method method) {
        String result = null;

        if (method != null) {
            final String fullTestClassName = method.getDeclaringClass().getSimpleName();
            result = fullTestClassName + "." + method.getName();
        } else {
            getLog().error("Method must not be null!");
        }

        return result;
    }

    /**
     * tear down the web driver.
     */
    @AfterSuite
    public static void oneTimeTearDown() throws Exception {
        driver.quit();
    }

    /**
     * The method returns log instance which is created in junit test classes.
     * 
     * @return the log
     */
    public abstract Logger getLog();
    
    
	public static boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		}
		catch (NoSuchElementException e) {
			return false;
		}
	}



}
