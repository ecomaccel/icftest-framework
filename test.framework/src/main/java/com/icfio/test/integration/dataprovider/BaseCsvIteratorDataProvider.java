/**
 * 
 */
package com.icfio.test.integration.dataprovider;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.testng.log4testng.Logger;

import com.icfio.test.integration.IntegrationTestUtil;

import au.com.bytecode.opencsv.CSVReader;


/**
 * Base CSV file data provider.
 * Iterator to CSV data file.
 * 
 * It uses called environment and method to find
 * corresponding file based on this convention:
 * <br/><br/>    
 *     src\test\resources\{environment}\{full package name}\{class-name}-{method-name}.csv 
 * <br/><br/>
 * Example of the determined file location for local environment and 
 * caller method com.icfio.integration.auth.IntTestBasicUserInfo.testInfo :
 * <br/><br/>
 *     src\test\resources\local\com\icfio\integration\auth\IntTestBasicUserInfo-testInfo.csv
 * <br/><br/>    
 * Result data contains the map with string keys values parameters from
 * corresponding CSV file and from common map. 
 * 
 * This class is not thread safe, but every callers method use new instance.
 * 
 * @author Andrey Gotchalk
 *
 */
public abstract class BaseCsvIteratorDataProvider implements Iterator<Object[]> {
    
    private CSVReader reader;

    /** The latest row we returned */
    private String[] last;
   
    public BaseCsvIteratorDataProvider(Method method, String env) throws IOException {
        /** get file path to CSV from environment, package, class and method */
        String csvFilePath = IntegrationTestUtil.buildPathAndFileNameToDataFile(method, env) + ".csv"; 

        reader = new CSVReader(new FileReader(csvFilePath));
        getLog().info("Start parsing CSV file: "+ csvFilePath);
    }   
    
    @Override
    public final boolean hasNext() {
        try {
            last = reader.readNext();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return last != null;
    } 
    
    /**
     * @return the Object[] representation of the next line
     */
    @Override
    public final Object[] next() {
        String[] next;
        if (last != null) {
            next = last;
        } else {
            next = getNextLine();
        }
        last = null;

        /** create result map that will be casted to Object[] */
        HashMap<String, String> map = new HashMap<String, String>();

        /** Add common map to result map. 
         *  Specific CSV may overwrite default value for the common map 
         **/
        map.putAll(getCommonMap());
        
        /** Add key values from next CSV line */
        map.putAll(parseLineToMap(next));
        
        return new Object[] { map };
    }
    
    /**
     * Parses the line to map.
     *
     * @param svals the string values from CSV line (parsed by CSVReader)
     * @return the correctly parsed and wrapped values as Map
     */
    private HashMap<String, String> parseLineToMap(String[] svals) {
        HashMap<String, String> map = new HashMap<String, String>();
        
        for (String keyValue : svals) {
            int indexEq = keyValue.indexOf('=');
            
            if (keyValue.isEmpty()) { 
                getLog().warn("Empty element in CSV line. Skipped");
            } else if (indexEq == -1) {
                getLog().error("Can't find = in the string: "+ keyValue);
            } else {
                String key = keyValue.substring(0, indexEq);
                String value = keyValue.substring(indexEq + 1);
                map.put(key, value);
            }
        }

        return map;
    }
    
    /**
     * Get the next line, or the current line if it's already there.
     * 
     * @return the line.
     */
    private String[] getNextLine() {
        if (last == null) {
            try {
                last = reader.readNext();
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }
        return last;
    } 
    
    @Override
    public final void remove() {
        throw new UnsupportedOperationException(); // N/A
    }
    
    /**
     * Gets the common map.
     *
     * @return the common map
     */
    public abstract Map<String, String> getCommonMap(); 
    
    
    /**
     * The method returns log instance which is created in concrete implementation classes.
     * 
     * @return the log
     */
    public abstract Logger getLog();

    
}
