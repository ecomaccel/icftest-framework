package com.icfio.test.integration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;

import org.testng.log4testng.Logger;

/**
 * Integration test utility class, contains common constants and methods.
 *
 * @author Andrey Gotchalk
 */
public final class IntegrationTestUtil {
    private static final Logger log = Logger.getLogger(IntegrationTestUtil.class);
    
    /**
     * This utility class cannot be instantiated.
     */
    private IntegrationTestUtil() {
    }
   
    /**
     * Default environment is 'local'
     */
    public static final String DEFAULT_ENV = "local";
    //public static final String DEFAULT_ENV = "stg"; 
    
	/**
	 * The Constant DEFAULT_URL. To be used if test is running without using of
	 * testSuite or current suite doesn't contain selenium.url value
	 */
    // used local liftmaster as current site for which it's developing tests now, you may change it to your default site
    public static final String DEFAULT_URL = "https://dealer-local.liftmaster.com/";   
    
    /**
     * User directory from the system properties.
     */
    private static final String usrDir = System.getProperty("user.dir");
    
    /**
     * File separator from the system properties.
     */
    public static final String fsep = System.getProperty("file.separator");
    
    /**
     * Path to the test resource directory.
     */
    public static final String testResourceDir = usrDir + fsep + "src" + fsep + "test" + fsep + "resources";
    
    /**
     * Builds the path and file name (without extension!)
     * to the test data file directory based on the environment, package,
     * class and method [based on convention].
     * 
     * @param method
     *            - currently executing test method
     * @param env
     *            - test environment
     * @return path to the data file directory for the given test method
     */
    public static String buildPathAndFileNameToDataFile(Method method, String env) {

        Class<?> cls = method.getDeclaringClass();
        String packageName = cls.getPackage().getName();
        String dirPlusPrefix = testResourceDir + fsep + env + fsep + packageName.replace(".", fsep);
        String fileName = cls.getSimpleName() + '-' + method.getName();
        
        return dirPlusPrefix + fsep + fileName;
    }
    
    /**
     * Loads properties from the given property file.
     *
     * @param pathToFile
     * @return properties loaded from the given file
     */
    public static Properties loadPropertiesFromFile(String pathToFile) {        
        Properties properties = new Properties();
        
        FileInputStream fInStr = null; 
        try {
            log.debug("Try to load properties from : " + pathToFile);
            fInStr = new FileInputStream(new File(pathToFile));
            properties.load(fInStr);
            fInStr.close();
            log.debug("File read done.");
        } catch (IOException ie) {
            log.error("Error reading file: " + pathToFile);
        } finally {
            try {
                if (null != fInStr) {
                    fInStr.close();
                }
            } catch (IOException e) {
                log.error("Error clossing file stream", e);
            }
        }
        
        return properties;
    }
    
    
}
