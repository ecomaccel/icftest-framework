package com.icfio.test.integration.dataprovider;

import static com.icfio.test.integration.IntegrationTestUtil.DEFAULT_ENV;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import com.icfio.test.integration.constant.TestSuiteKeyNameE;

/**
 * Generic Provider contains method providing test data from 
 * different data files.
 * 
 * Test class should have such annotation:
 * <br/>
 * <code> @Test(dataProviderClass = GenericDataProvider.class)</code>
 * <br/><br/>
 * and each test method should indicate which data provider to use:<br/>
 * <code> @Test(dataProvider = "csv-data-provider")
 * <br/>  @Test(dataProvider = "props-data-provider")
 * <br/>  @Test(dataProvider = "csv-props-data-provider") 
 * </code><br/><br/>
 * Look at the details for each data provider in their java doc description. 
 *
 * @author Andrey.Gotchalk@Compuware.com
 * 
 */
public final class DataProviderFactory {

    /**
     * This class cannot be instantiated.
     */
    private DataProviderFactory() {
        
    }
    
    /**
     * Provides test data form the CSV file.
     *  
     * It can be useful for test case that need test with different sets of test data.
     * The same test method will be run for each CSV line.  
     * 
     * It uses called environment and method to find
     * corresponding file based on this convention:
     * <br/><br/>     
     *     src\test\resources\{environment}\{full package name}\{class-name}-{method-name}.csv 
     * <br/><br/> 
     * Example of the determined file location for local environment and 
     * caller method com.icfio.integration.auth.IntTestBasicUserInfo.testInfo :
     * <br/><br/> 
     *     src\test\resources\local\com\icfio\integration\auth\IntTestBasicUserInfo-testInfo.csv
     * <br/><br/>     
     * Result data contains the map with string keys values parameters from
     * corresponding CSV file and from common properties. 
     * For use this data the test method should have this annotation:
     * <br/><br/>    
     * <code> @Test(dataProvider = "csv-data-provider") </code>
     * <br/><br/>
     * and the map variable in method parameters like this:
     * <br/><br/> 
     * <code> public final void testInfo(HashMap<String, String> dpMap) </code>
     * 
     * @param method the method
     * @param context the context
     * @return the csv data provider
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @DataProvider(name = "csv-data-provider")
    public static Iterator<Object[]> getCsvFileDataProvider(Method method, ITestContext context) throws IOException {
        return new CsvFileDataProvider(method, getTestEnv(context));
    }

    /**
     * Provides test data form the property file.
     * 
     * It can be useful for one iteration scenario that has too many keys values.  
     * 
     * It uses called environment and method to find
     * corresponding file based on this convention:
     * <br/><br/>     
     *     src\test\resources\{environment}\{full package name}\{class-name}-{method-name}.properties 
     * <br/><br/> 
     * Example of the determined file location for local environment and 
     * caller method com.icfio.integration.auth.IntTestBasicUserInfo.testInfoExt :
     * <br/><br/> 
     *     src\test\resources\local\com\icfio\integration\auth\IntTestBasicUserInfo-testInfoExt.properties
     * <br/><br/>     
     * Result data contains the map with string keys values parameters from
     * corresponding properties file and from common properties. 
     * For use this data the test method should have this annotation:
     * <br/><br/>     
     * <code> @Test(dataProvider = "props-data-provider") </code>
     * <br/><br/> 
     * and the map variable in method parameters like this:
     * <br/><br/>
     * <code> public final void testInfoExt(HashMap<String, String> dpMap) </code> 
     * 
     * @param method
     *            the method
     * @param context
     *            the TestNG context from XML suite file 
     * @return the props data provider
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @DataProvider(name = "props-data-provider")
    public static Object[][] getPropertyFileDataProvider(Method method, ITestContext context) throws IOException {
        return PropertyFileDataProvider.getData(method, getTestEnv(context));
    }
    
    /**
     * Provides test data form the CSV and properties files.
     *  
     * It can be useful for test case that need test with different sets of test data
     * and each test has too many similar keys values like locators.
     * The same test method will be run for each CSV line.  
     * 
     * It uses called environment and method to find
     * corresponding csv and properties files based on this convention(example for csv):
     * <br/><br/>     
     *     src\test\resources\{environment}\{full package name}\{class-name}-{method-name}.csv 
     * <br/><br/> 
     * Example of the determined file location for local environment and 
     * caller method com.icfio.integration.auth.IntTestBasicUserInfo.testInfo :
     * <br/><br/> 
     *     src\test\resources\local\com\icfio\integration\auth\IntTestBasicUserInfo-testInfo.csv
     * <br/><br/>     
     * Result data contains the map with string keys values parameters from
     * corresponding CSV file, properties file and from common.properties. 
     * For use this data the test method should have this annotation:
     * <br/><br/>    
     * <code> @Test(dataProvider = "csv-props-data-provider") </code>
     * <br/><br/>
     * and the map variable in method parameters like this:
     * <br/><br/> 
     * <code> public final void testInfo(HashMap<String, String> dpMap) </code>
     * 
     * @param method the method
     * @param context the context
     * @return the csv data provider
     * @throws IOException Signals that an I/O exception has occurred.
     */    
    @DataProvider(name = "csv-props-data-provider")
    public static Iterator<Object[]> getPropsCsvFileDataProvider(Method method, ITestContext context)
            throws IOException {
      return new CsvPropsFilesDataProvider(method, getTestEnv(context));
    }
    
    /**
     * Gets the name of test environment from TestNG context.
     * 
     * @param context
     *            - test NG context
     * @return the test environment if provided in context, use DEFAULT
     *         otherwise
     */
    private static String getTestEnv(ITestContext context) {
        String testEnv = context.getCurrentXmlTest().getParameter(TestSuiteKeyNameE.TEST_ENV.getKeyName()); 
        if (StringUtils.isEmpty(testEnv)) {
            testEnv = DEFAULT_ENV;
        }
        return testEnv;
    }
    
    
}
