/**
 * 
 */
package com.icfio.test.integration.constant;

/**
 * Contains TestNG suite key names.
 * 
 */
public enum TestSuiteKeyNameE {
    SELENIUM_BROWSER ("selenium.browser"),
    
    SELENIUM_BROWSER_VERSION ("selenium.browser.version"),
    
    SELENIUM_URL ("selenium.url"),
    
    SELENIUM_PROXY ("selenium.proxy"),
    
    SELENIUM_PROXY_AUTODETECT("selenium.proxy.autodetect"),
    
    TEST_ENV ("test.env"),
    
    SELENIUM_REMOTE_HUB_LINK ("selenium.remote.hub.link");
    
    private String keyName;
    
    private TestSuiteKeyNameE(String keyName) {
        this.keyName = keyName;
    }
    
    public String getKeyName() {
        return keyName;
    }
}
