package com.icfio.test.integration.dataprovider;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.testng.log4testng.Logger;

import com.icfio.test.integration.IntegrationTestUtil;

/** 
 * Reads file "common.properties" once and provides its content as properties or as map.
 * 
 * @author Andrey Gotchalk 
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public final class CommonPropsFlyweight {
    private static final Logger LOG = Logger.getLogger(CommonPropsFlyweight.class);
    
    /**
     * Keeps test common properties loaded from file.
     */
    private static Properties properties = null;
        
    /**
     * Keeps test common properties loaded from file as map.
     */
    private static Map<String, String> map = null; 
    
    /**
     * Default commong property file name.
     */
    private static final String COMMON_PROPS_FILE = "common.properties";
    
    /**
     * Private constructor, this class cannot be instantiated.
     */
    private CommonPropsFlyweight() {
        
    }
    
    /** 
     * Static initialisation of class. Thread safe.
     * Will be run only once before any call of this class so provide singleton pattern
     * (Properties file will be read only once) 
     */
    static {
        
        String fileName = IntegrationTestUtil.testResourceDir + IntegrationTestUtil.fsep + COMMON_PROPS_FILE;
        
        /* Load properties from file */
        Properties propertiesFromFile = IntegrationTestUtil.loadPropertiesFromFile(fileName);
        
        if(null != propertiesFromFile && propertiesFromFile.entrySet().size() > 0) {
            LOG.info("Number of properties loaded: " + propertiesFromFile.entrySet().size());
            
            properties = propertiesFromFile;
            map = new HashMap<String, String>((Map) propertiesFromFile); 
        } else {
            LOG.info("No properties loaded from common.properties file.");
            
            properties = new Properties();
            map = new HashMap<String, String>();
        }
        
        LOG.info("Common properties successfully loaded.");
        for(Entry<Object, Object> e : properties.entrySet()) {
            LOG.debug(e.getKey() + " : " + e.getValue());
        }
    }
    
    /**
     * Gets the properties as map.
     *
     * @return the properties as map
     */
    public static Map<String, String> getPropertiesAsMap() {
        return map;
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    public static Properties getProperties() {
        return properties;
    }
    
}
