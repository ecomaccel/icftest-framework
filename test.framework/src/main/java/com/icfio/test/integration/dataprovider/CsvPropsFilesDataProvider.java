/**
 * 
 */
package com.icfio.test.integration.dataprovider;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.testng.log4testng.Logger;

import com.icfio.test.integration.IntegrationTestUtil;

/**
 * CSV and properties files data provider.
 * It's the combination of functionalities of 
 * CsvFileDataProvider and PropertyFileDataProvider.
 * 
 * It uses called environment and method to find
 * corresponding csv and properties files based on this convention(example for csv):
 * <br/><br/>    
 *     src\test\resources\{environment}\{full package name}\{class-name}-{method-name}.csv 
 * <br/><br/>
 * Example of the determined file location for local environment and 
 * caller method com.icfio.integration.auth.IntTestBasicUserInfo.testInfo :
 * <br/><br/>
 *     src\test\resources\local\com\icfio\integration\auth\IntTestBasicUserInfo-testInfo.csv
 * <br/><br/>    
 * Result data contains the map with string keys values parameters from
 * corresponding CSV file, from common properties and from corresponding properties file. 
 * 
 * This class is not thread safe, but every callers method use new instance.
 *  
 * @author Andrey Gotchalk
 *
 */
public final class CsvPropsFilesDataProvider extends BaseCsvIteratorDataProvider {
    protected static final Logger log = Logger.getLogger(CsvPropsFilesDataProvider.class);
    HashMap<String, String> propertiesMap;
    
    /**
     * @param method
     * @param env
     * @throws IOException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public CsvPropsFilesDataProvider(Method method, String env) throws IOException {
        super(method, env);
        
        /** get file path to properties from environment, package, class and method */
        String propFilePath = IntegrationTestUtil.buildPathAndFileNameToDataFile(method, env) 
             + ".properties";

        /** get properties from file */
        Properties properties = IntegrationTestUtil.loadPropertiesFromFile(propFilePath);
        /** cast properties to map */
        propertiesMap = new HashMap<String, String>((Map) properties);
    }
    
    /**
     * Gets the common map and corresponding to test properties map.
     *
     * @return the common map
     */
    @Override
    public Map<String, String> getCommonMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        
        /** add common key values map. */
        map.putAll(CommonPropsFlyweight.getPropertiesAsMap());
        
        /** add keys values from properties file. */
        map.putAll(propertiesMap);
        getLog().debug("Result common map size (with common properties): " + map.size());        
        
        return map;
    }
    
    @Override
    public Logger getLog() {
        return log;
    }

}
