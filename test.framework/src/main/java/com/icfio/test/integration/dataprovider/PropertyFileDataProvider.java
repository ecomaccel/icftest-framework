/**
 * 
 */
package com.icfio.test.integration.dataprovider;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.testng.log4testng.Logger;

import com.icfio.test.integration.IntegrationTestUtil;

/**
 * Property file data provider.
 * Contains method to read property data file and present it to TestNG as Object[][].
 * 
 * It uses called environment and method to find
 * corresponding file based on this convention:
 * <br/><br/>     
 *     src\test\resources\{environment}\{full package name}\{class-name}-{method-name}.properties 
 * <br/><br/> 
 * Example of the determined file location for local environment and 
 * caller method com.icfio.integration.auth.IntTestBasicUserInfo.testInfoExt :
 * <br/><br/> 
 *     src\test\resources\local\com\icfio\integration\auth\IntTestBasicUserInfo-testInfoExt.properties
 * <br/><br/>     
 * Result data contains the map with string keys values parameters from
 * corresponding properties file and from common properties. 
 * 
 * This class is not thread safe, but every callers method use new instance.
 * 
 * @author Andrey.Gotchalk@Compuware.com
 *
 */
public final class PropertyFileDataProvider {
    protected static final Logger log = Logger.getLogger(PropertyFileDataProvider.class);
    
    private PropertyFileDataProvider() {
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Object[][] getData(Method method, String env) throws IOException {
        
        /** get file path to properties from environment, package, class and method */
        String propFilePath = IntegrationTestUtil.buildPathAndFileNameToDataFile(method, env) 
            + ".properties";

        /** get properties from file */
        Properties properties = IntegrationTestUtil.loadPropertiesFromFile(propFilePath);
 
        /** prepare result map */
        HashMap<String, String> map = new HashMap<String, String>();
        
        /** add common keys values to result map. 
         *  Specific properties file may overwrite default value for the common key 
         **/
        map.putAll(CommonPropsFlyweight.getPropertiesAsMap());

        /** add keys values from properties file */
        map.putAll(new HashMap<String, String>((Map) properties)); 
        log.debug("Result map size (with common map): " + map.size());
        
        /** wrap result map as Object[][] according to TesnNG data provider specification */
        Object[][] result = new Object[1][];
        result[0] = new Object[] { map };
        
        return result;
    }
    
    
}
