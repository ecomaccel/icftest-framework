package com.icfio.test.integration.dataprovider;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;

import org.testng.log4testng.Logger;

/**
 * CSV file data provider.
 * Iterator to CSV data file.
 * 
 * It uses called environment and method to find
 * corresponding file based on this convention:
 * <br/><br/>    
 *     src\test\resources\{environment}\{full package name}\{class-name}-{method-name}.csv 
 * <br/><br/>
 * Example of the determined file location for local environment and 
 * caller method com.icfio.integration.auth.IntTestBasicUserInfo.testInfo :
 * <br/><br/>
 *     src\test\resources\local\com\icfio\integration\auth\IntTestBasicUserInfo-testInfo.csv
 * <br/><br/>    
 * Result data contains the map with string keys values parameters from
 * corresponding CSV file and from common properties. 
 * 
 * This class is not thread safe, but every callers method use new instance.
 * 
 * @author Andrey Gotchalk
 */
public class CsvFileDataProvider extends BaseCsvIteratorDataProvider {
    private static final Logger log = Logger.getLogger(CsvFileDataProvider.class);
    
    public CsvFileDataProvider(Method method, String env) throws IOException {
        super(method, env);
    }

    @Override
    public final Map<String, String> getCommonMap() {
        /** get common key values map .    */
        return CommonPropsFlyweight.getPropertiesAsMap();
    }

    @Override
    public final Logger getLog() {
        return log;
    }
}
