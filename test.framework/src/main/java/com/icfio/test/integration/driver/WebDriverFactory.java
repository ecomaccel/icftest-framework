package com.icfio.test.integration.driver;

import java.net.URL;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.log4testng.Logger;

import com.icfio.test.integration.constant.BrowserTypeE;
import com.icfio.test.integration.constant.TestSuiteKeyNameE;


/**
 * WebDriver static factory.
 */
public final class WebDriverFactory {
	private static final Logger LOG = Logger.getLogger(WebDriverFactory.class);

	/**
	 * Creates WebDriver based on the TestNG context.
	 * 
	 * @param context
	 *            - test NG context [contains web browser type property]
	 * @return WebDriver based on test context, or default [FF] if context does not contain browser type parameter.
	 * @throws WebDriverException
	 */
	public static WebDriver createWebDriver(ITestContext context) throws WebDriverException {
		String seleniumBrowser = context.getCurrentXmlTest().getParameter(TestSuiteKeyNameE.SELENIUM_BROWSER.getKeyName());
		LOG.info("Create web driver for browser: " + seleniumBrowser);
		String remoteHubLink = context.getCurrentXmlTest().getParameter(TestSuiteKeyNameE.SELENIUM_REMOTE_HUB_LINK.getKeyName());
		LOG.info("Selenium remote driver: " + remoteHubLink);
		boolean isRemoteDriver = false;
		if (StringUtils.isNotBlank(remoteHubLink)) {
			isRemoteDriver = true;
			LOG.info("isRemoteDriver: " + isRemoteDriver);
		}
		BrowserTypeE b;
		try {
			b = BrowserTypeE.valueOf(seleniumBrowser);
		}
		catch (Exception e) {
			b = BrowserTypeE.FF;
			LOG.error("Browser type was not found, Firefox is used by default: ");
		}
		WebDriver webDriver = null;
		DesiredCapabilities browserCapabilities = BrowserCapabilitiesFactory.createBrowserCapabilities(b, context);
		if (isRemoteDriver) {
			try {
				webDriver = new RemoteWebDriver(new URL(StringUtils.trimToEmpty(remoteHubLink)), browserCapabilities);
			}
			catch (Exception e) {
				LOG.error("Exception during create of RemoteWebDriver: " + e.getMessage());
				throw (new WebDriverException(e));
			}
		}
		else if (null != b) {
			switch (b) {
				case FF:
					webDriver = new FirefoxDriver(browserCapabilities);
					break;
				case IE:
					// system property defined by maven plugin
					if (LOG.isDebugEnabled()) {
						LOG.debug("Check system property value for webdriver.ie.driver:" + System.getProperty("webdriver.ie.driver"));
					}
					// use for debug when start test suite without maven
					if (StringUtils.isEmpty(System.getProperty("webdriver.ie.driver"))) {
						String binarypath = System.getProperty("user.dir")
						        + "\\src\\test\\resources\\drivers\\selenium_standalone_binaries\\windows\\internetexplorer\\64bit\\IEDriverServer.exe";
						if (LOG.isDebugEnabled()) {
							LOG.debug("Set system property value for webdriver.ie.driver to :" + binarypath);
						}
						System.setProperty("webdriver.ie.driver", binarypath); 	
					}
					webDriver = new InternetExplorerDriver(browserCapabilities);
					break;
				case HtmlUnit:
					webDriver = new HtmlUnitDriver(browserCapabilities);
					break;
				case Chrome:
					// system property defined by maven plugin
					if (LOG.isDebugEnabled()) {
						LOG.debug("Check system property value for webdriver.chrome.driver:" + System.getProperty("webdriver.chrome.driver"));
					}
					// System.setProperty("webdriver.chrome.driver", "C:\\ICF\\selenium\\drivers\\chromedriver.exe");
					webDriver = new ChromeDriver();
					break;
				case Opera:
					// system property defined by maven plugin
					if (LOG.isDebugEnabled()) {
						LOG.debug("Check system property value for webdriver.opera.driver:" + System.getProperty("webdriver.opera.driver"));
					}
					// looks like bug in OperaDriver with find binary, try later OperaService if Opera really needed
					webDriver = new OperaDriver();
					break;
			}
		}
		if (null != webDriver) {
			LOG.info("Successfully created: " + webDriver.getClass().getSimpleName());
		}
		return webDriver;
	}

	private WebDriverFactory() {
	}
}
