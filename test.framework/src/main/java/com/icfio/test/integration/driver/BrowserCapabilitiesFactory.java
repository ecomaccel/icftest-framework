/**
 * 
 */
package com.icfio.test.integration.driver;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.log4testng.Logger;

import com.icfio.test.integration.constant.BrowserTypeE;
import com.icfio.test.integration.constant.TestSuiteKeyNameE;

/**
 * Browser capabilities static factory.
 * 
 */
public final class BrowserCapabilitiesFactory {
    private static final Logger LOG = Logger.getLogger(BrowserCapabilitiesFactory.class);

    /**
     * Creates DesiredCapabilities based on the browser type.
     * 
     * @param browserType
     * @param testContext
     * @return new DesiredCapabilities for the given browser.
     */
    public static DesiredCapabilities createBrowserCapabilities(BrowserTypeE browserType, ITestContext testContext) {
        DesiredCapabilities desiredCapabilities = null;
        
        switch (browserType) {
        case FF:
            desiredCapabilities = generateFFCapabilities(testContext) ;
            break;
        case IE:
            desiredCapabilities = generateIECapabilities(testContext) ;
            break;
        case HtmlUnit:
            desiredCapabilities = generateHtmlUnitCapabilities(testContext) ;
            break;
        case Opera:
            desiredCapabilities = generateOperaCapabilities(testContext) ;
            break;            
        }
        
        if (null != desiredCapabilities) {
            setCommonCapabilities(desiredCapabilities, testContext);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Use desired capabilities: " + desiredCapabilities);
            }
        }
        
        return desiredCapabilities;
    }
    
    private static void setCommonCapabilities(DesiredCapabilities dC, ITestContext testContext) {        
        String seleniumProxy = testContext.getCurrentXmlTest().getParameter(
                TestSuiteKeyNameE.SELENIUM_PROXY.getKeyName());
        
        /* define proxy */
        Proxy proxy = new Proxy();
        if (StringUtils.isNotEmpty(seleniumProxy)) {
            proxy.setHttpProxy(seleniumProxy);
            proxy.setSslProxy(seleniumProxy);

            dC.setCapability(CapabilityType.PROXY, proxy);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Set HtmlProxy: " + seleniumProxy);
            }
        } else {
            String autoDetect = testContext.getCurrentXmlTest().getParameter(
                    TestSuiteKeyNameE.SELENIUM_PROXY_AUTODETECT.getKeyName());           
            if (StringUtils.isNotEmpty(autoDetect)) {
                /* The boolean returned represents the value true if the string argument is not null and is equal,
                 * ignoring case, to the string "true".
                 * In all other cases it'll return false, since no exception are declared to be thrown */
                proxy.setAutodetect(Boolean.parseBoolean(autoDetect));
                dC.setCapability(CapabilityType.PROXY, proxy);
            } else {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("HtmlProxy setting skipped.");
                }
            }
        }

        dC.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        dC.setJavascriptEnabled(true);

    }
    
    /**
     * Gets a capability for FireFox browser along with specific
     * proxy settings and untrust SSL settings. Will provide basic
     * capabilities for browser. Does not have fine grained level of
     * configuration like JS engine version, OS, etc.
     * 
     * @return the desired capabilities
     */
    private static DesiredCapabilities generateFFCapabilities(ITestContext testContext) {

        if (LOG.isDebugEnabled()) {
            LOG.debug("generate FF Capabilities");
        }

        DesiredCapabilities cap = null;
        cap = DesiredCapabilities.firefox();
        
        // use stored profile
//      File ffProfileDir = new File("c:\\java\\testframework\\FFprofile");
//      FirefoxProfile profile = new FirefoxProfile(ffProfileDir);
//
//      //some more prefs:
//      profile.setPreference("app.update.enabled", false);
//      profile.setPreference("browser.tabs.autoHide", true);
//      cap.setCapability(FirefoxDriver.PROFILE, profile);
        
        String version = getBrowserVersionFromContext(testContext);
        if (StringUtils.isNotEmpty(getBrowserVersionFromContext(testContext))) {
            cap.setVersion(version);
        } 

        if (LOG.isDebugEnabled()) {
            LOG.debug("FF Capabilities successfully generated");
        }
        return cap;
    }

    /**
     * Gets a capability for HtmlUnit browser. Will provide basic
     * capabilities for browser. Does not have fine grained level of
     * configuration like browser version, JS engine version, OS, etc.
     * 
     * @return the desired capabilities
     */
    private static DesiredCapabilities generateHtmlUnitCapabilities(ITestContext testContext) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("generateHtmlUnitCapabilities starting... ");
        }

        DesiredCapabilities cap = null;
        cap = DesiredCapabilities.htmlUnit();

        if (LOG.isDebugEnabled()) {
            LOG.debug("HtmlUnit Capabilities successfully generated");
        }
        return cap;
    }

    /**
     * Gets a capability for InternetExplorer (IE) browser. Will provide basic
     * capabilities for browser. Does not have fine grained level of
     * configuration like JS engine version, OS, etc.
     * 
     * @return the desired capabilities
     */
    private static DesiredCapabilities generateIECapabilities(ITestContext testContext) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("generateIECapabilities starting... ");
        }

        DesiredCapabilities cap = null;
        cap = DesiredCapabilities.internetExplorer();
        cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		cap.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);

        String version = getBrowserVersionFromContext(testContext);
        if (StringUtils.isNotEmpty(getBrowserVersionFromContext(testContext))) {
            cap.setVersion(version);
        } 
        
        if (LOG.isDebugEnabled()) {
            LOG.debug("IE Capabilities for Windows successfully generated");
        }
        return cap;
    }
    
	private static DesiredCapabilities generateOperaCapabilities(ITestContext testContext) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("generateIECapabilities starting... ");
		}

		DesiredCapabilities cap = null;
		cap = DesiredCapabilities.opera();
		if (LOG.isDebugEnabled()) {
			LOG.debug("Check system property value for webdriver.opera.driver:" + System.getProperty("webdriver.opera.driver"));
		}
		cap.setCapability("opera.binary", System.getProperty("webdriver.opera.driver"));

		if (LOG.isDebugEnabled()) {
			LOG.debug("Opera Capabilities successfully generated");
		}
		return cap;
	}

    private BrowserCapabilitiesFactory() {
    }
    
    /**
     * Gets the browser version from context.
     *
     * @param testContext the test context
     * @return the browser version from context
     */
    private static String getBrowserVersionFromContext(ITestContext testContext) {
        return testContext.getCurrentXmlTest().getParameter(
                TestSuiteKeyNameE.SELENIUM_BROWSER_VERSION.getKeyName());
    }

}
